package org.techern.minecraft.supersnowballs;

import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.SnowballItem;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * A {@link Mod} to extend on the Vanilla {@link SnowballItem}
 *
 * @since 0.8
 */
@Mod("supersnowballs")
public class SuperSnowballsMod
{

    /**
     * The {@link Logger}
     *
     * @since 0.8
     */
    private static final Logger LOGGER = LogManager.getLogger();

    /**
     * Creates a new {@link SuperSnowballsMod}
     *
     * @since 0.8
     */
    public SuperSnowballsMod() {
        // Register ourselves for server and other game events we are interested in
        MinecraftForge.EVENT_BUS.register(this);
    }

    /**
     * A utility class for {@link RegistryEvent}s
     *
     * @since 0.8
     */
    @Mod.EventBusSubscriber(bus=Mod.EventBusSubscriber.Bus.MOD)
    public static class RegistryEvents {

        /**
         * Called when the {@link RegistryEvent} processes {@link Item}s
         *
         * @param blockRegistryEvent The {@link RegistryEvent}
         * @since 0.8
         */
        @SubscribeEvent
        public static void onItemsRegistry(final RegistryEvent.Register<Item> blockRegistryEvent) {
            // register a new block here
            LOGGER.info("Attempting to reregister snowballs");

            blockRegistryEvent.getRegistry().register(new SnowballItem((new Item.Properties()).maxStackSize(64).group(ItemGroup.MISC)).setRegistryName("minecraft:snowball"));

        }
    }
}
